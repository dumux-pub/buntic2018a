# dune-common
# releases/2.5 # e30dcbfe43224f13c750dd6e2aa9cf0b00a1fbbf # 2018-11-08 14:58:45 +0000 # Robert K
git clone https://gitlab.dune-project.org/core/dune-common.git
cd dune-common
git checkout releases/2.5
git reset --hard e30dcbfe43224f13c750dd6e2aa9cf0b00a1fbbf
cd ..

# dune-geometry
# releases/2.5 # d45e7d6040b1bd61b846a739a9645feb4fe63d4c # 2018-04-25 12:06:49 +0200 # Ansgar Burchardt
git clone https://gitlab.dune-project.org/core/dune-geometry.git
cd dune-geometry
git checkout releases/2.5
git reset --hard d45e7d6040b1bd61b846a739a9645feb4fe63d4c
cd ..

# dune-localfunctions
# releases/2.5 # c15e2e6126ce48c3eea6f3e437463adf637445ff # 2018-04-25 12:07:31 +0200 # Ansgar Burchardt
git clone https://gitlab.dune-project.org/core/dune-localfunctions.git
cd dune-localfunctions
git checkout releases/2.5
git reset --hard c15e2e6126ce48c3eea6f3e437463adf637445ff
cd ..

# dune-grid
# releases/2.5 # 9731b83945271258aed87caa0ba53796b2a0d3c7 # 2018-04-25 12:07:02 +0200 # Ansgar Burchardt
git clone https://gitlab.dune-project.org/core/dune-grid.git
cd dune-grid
git checkout releases/2.5
git reset --hard 9731b83945271258aed87caa0ba53796b2a0d3c7
cd ..

# dune-typetree
# releases/2.5 # a6d7ce078e2fe2d1f5ac8d9468b609b58e4dc28b # 2018-04-25 12:07:41 +0200 # Ansgar Burchardt
git clone https://gitlab.dune-project.org/staging/dune-typetree.git
cd dune-typetree
git checkout releases/2.5
git reset --hard a6d7ce078e2fe2d1f5ac8d9468b609b58e4dc28b
cd ..

# dune-istl
# releases/2.5 # 2b2f2e8c50f7a77d8ba27860015a6018aaa255d9 # 2018-04-25 12:07:21 +0200 # Ansgar Burchardt
git clone https://gitlab.dune-project.org/core/dune-istl.git
cd dune-istl
git checkout releases/2.5
git reset --hard 2b2f2e8c50f7a77d8ba27860015a6018aaa255d9
cd ..

# dune-functions
# releases/2.5 # 1a403d371677b2c37a61bc99f80b4bc1c958abe4 # 2018-04-25 10:06:37 +0000 # Ansgar Burchardt
git clone https://gitlab.dune-project.org/staging/dune-functions.git
cd dune-functions
git checkout releases/2.5
git reset --hard 1a403d371677b2c37a61bc99f80b4bc1c958abe4
cd ..

# dune-uggrid
# releases/2.5 # 8fe883e99c58c9f0c2f92457d546a0ac9f5a9bf9 # 2018-04-25 # Ansgar Burchardt
git clone https://gitlab.dune-project.org/staging/dune-uggrid.git
cd dune-uggrid
git checkout releases/2.5
git reset --hard 8fe883e99c58c9f0c2f92457d546a0ac9f5a9bf9
cd ..

# dumux
# feature/supg # 66cde6b4ad3ba26e585b614d977190aed45eed01 # 2019-03-28 11:19 # Ivan Buntic
git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git
cd dumux
git checkout feature/supg
git reset --hard 66cde6b4ad3ba26e585b614d977190aed45eed01
cd ..

# Buntic2018a, pub-module
# master # 79bfbe2a61be7da398017ff1a4beb5e27ea453c0 # 2019-03-28 10:56 # Ivan Buntic
git clone https://git.iws.uni-stuttgart.de/dumux-pub/buntic2018a.git
cd buntic2018a
git reset --hard 79bfbe2a61be7da398017ff1a4beb5e27ea453c0
cd ..

### Run dunecontrol
./dune-common/bin/dunecontrol --opts=dumux/optim.opts all






