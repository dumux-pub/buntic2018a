
install(FILES
        boxgeometryhelper.hh
        darcyslaw.hh
        elementfluxvariablescache.hh
        elementvolumevariables.hh
        fickslaw.hh
        fourierslaw.hh
        fvelementgeometry.hh
        globalfluxvariablescache.hh
        globalfvgeometry.hh
        globalvolumevariables.hh
        stencils.hh
        subcontrolvolumeface.hh
        subcontrolvolume.hh
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dumux/discretization/box)
