
install(FILES
        adaptionhelper.hh
        gridadapt.hh
        gridadaptindicatordefault.hh
        gridadaptinitializationindicator.hh
        gridadaptproperties.hh
        gridadaptpropertydefaults.hh
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dumux/implicit/adaptive)
