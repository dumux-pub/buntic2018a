add_subdirectory("mpfa")

install(FILES
        assembler.hh
        assemblymap.hh
        elementboundarytypes.hh
        localjacobian.hh
        localresidual.hh
        properties.hh
        propertydefaults.hh
        DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dumux/implicit/cellcentered)
