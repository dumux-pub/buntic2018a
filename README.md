Summary
=======

This is the DuMux module containing the code for reproducing the results published in:
I. Buntic
Investigation of the SUPG method for the Navier-Stokes equations
Bachelor's thesis 2018, Institut für Wasser- und Umweltsystemmodellierung, Universitt Stuttgart


Installation
============
The module can be built like any other DUNE module. For building and running the executables, please navigate to the folder containing
the source files.

You need to have gcc/c++ 5 installed to compile and run this module.

The most simple way to install the module is to create a new folder and execute the file [installBuntic2018a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/buntic2018a/raw/master/installBuntic2018a.sh)

```bash
mkdir -p Buntic2018a && cd Buntic2018a
wget -q https://git.iws.uni-stuttgart.de/dumux-pub/buntic2018a/raw/master/installBuntic2018a.sh
chmod +x installBuntic2018a.sh && ./installBuntic2018a.sh
```

For a detailed information on installation have a look at the DuMuX installation guide or use the DuMuX handbook, chapter 2.

Installation with Docker 
========================

Create a new folder in your favourite location and download the container startup script to that folder.
```bash
mkdir Buntic2018a
cd Buntic2018a
wget https://git.iws.uni-stuttgart.de/dumux-pub/buntic2018a/-/raw/master/docker_buntic2018a.sh
```

Open the Docker Container by running
```bash
bash docker_buntic2018a.sh open
```

Applications
============

The results shown in the publication are obtained by compiling and running the program from the source files in test/freeflow/stokes_fem/. One may build and execute them as follows:

```bash
cd buntic2018a/build-cmake/test/freeflow/stokes_fem
make test_stokesfemChannel
make test_stokesfemDrivenCavity
./test_stokesfemChannel test_stokesfemChannel.input
./test_stokesfemDrivenCavity test_stokesfemDrivenCavityRe1.input
```
In the docker container, it is not nessesary to compile the source codes as all the binaries are shipped with the container.

Output
======

Run test_stokesfemDrivenCavity or test_stokesfemChannel with the respective ini-files, which can be found in the same subfolder as the source files (You will need to copy the input-files from the "test" folder into the "build-cmake" folder):

__Figure 6.2. - small Reynolds number__:
    - use "test_stokesfemChannel.input" with "InletVelocity = 1"
    - stabilization terms can be turned on by setting "IsStabilized = 1"
    - bad convergence due to pressure oscillations

__Figure 6.5. - driven cavity__:  
    - use "test_stokesfemDrivenCavityRe1.input"  
    - stabilization terms can be turned on by setting "IsStabilized = 1"


Used Versions and Software
==========================
For an overview of the used versions of the DUNE and DuMuX modules, please have a look at
[installBuntic2018a.sh](https://git.iws.uni-stuttgart.de/dumux-pub/buntic2018a/raw/master/installBuntic2018a.sh).     

